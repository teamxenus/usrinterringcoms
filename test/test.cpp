#include <cinttypes>
#include <stdio.h>
#include <sys/types.h>

#include <LibIRC.h>
#include <XenusDelegatedCalls.h>
#include <XenusFile.h>

int main()
{

	uint32_t id;
	void * ahh;
	
	if (!XenusGetFunction("testfunction", &id))
		return -1;
	
	ahh = (void *)XenusCallFunction(id, (uint64_t)"hello world", sizeof("hello world"), 3, 4, 5, 6, 7, 8, 9, 10);
	
	printf(":: %llx\n", (long long unsigned int)ahh);
	
	printf("%llx, %llx\n", *(long long unsigned int*)(size_t(ahh) + 0), *(long long unsigned int*)(size_t(ahh) + 4096));
	
	XenusFile file;
	
	XenusOpen(&file, 1);
	XenusWrite(file, "hello kernel", sizeof("hello kernel"), 69);
	
	char shit[2];
	size_t idx = 0;
	while (XenusRead(file, shit, 2, idx))
	{
		
		printf("'%s'", shit);
		idx += 2;
	}
	
	return 0;
}