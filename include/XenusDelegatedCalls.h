typedef struct xenus_attention_sys
{
	uint32_t attention_id;
	uint64_t arg_alpha;
	uint64_t arg_bravo;
	uint64_t arg_charlie;
	uint64_t arg_delta;
    uint64_t arg_echo;
    uint64_t arg_foxy;
    uint64_t arg_golf;
    uint64_t arg_hotel;
    uint64_t arg_injina;
    uint64_t arg_juliet;
	uint64_t response;    
} * xenus_attention_sys_p,
  * xenus_attention_sys_ref,
    xenus_attention_sys_t;

IRC_SYM bool     XenusGetFunction(const char * fn, uint32_t * id);
IRC_SYM uint32_t XenusCallFunctionEx(xenus_attention_sys_ref call);
//IRC_SYM uint32_t XenusCallFunction(uint32_t id, uint64_t a, uint64_t b, uint64_t c, uint64_t d);


static inline uint64_t XenusCallFunction(uint32_t id, uint64_t a, uint64_t b, uint64_t c, uint64_t d, uint64_t e, uint64_t f, uint64_t g, uint64_t h, uint64_t i, uint64_t j)
{
	xenus_attention_sys_t call;
	call.attention_id   = id;
	call.arg_alpha      = a;
	call.arg_bravo      = b;
	call.arg_charlie    = c;
	call.arg_delta      = d;
	call.arg_echo       = e;
	call.arg_foxy       = f;
	call.arg_golf       = g;
	call.arg_hotel      = h;
	call.arg_injina     = i;
	call.arg_juliet     = j;
	XenusCallFunctionEx(&call);
	return call.response;
}

