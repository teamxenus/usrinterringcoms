
#define PSEUDOFILE_ERROR_CB_ERROR -1
#define PSEUDOFILE_ERROR_NO_HANDLER -2
#define PSEUDOFILE_ERROR_MEM_ERROR -3

#define PSEUDOFILE_ERROR_ILLEGAL_PARAM -10

typedef void * XenusFile;	

bool    IRC_SYM XenusOpen(XenusFile * file, const void * blob);
bool    IRC_SYM XenusOpen(XenusFile * file, uint64_t idx);
ssize_t IRC_SYM XenusRead(XenusFile file, void * buffer, size_t length, loff_t offset);
ssize_t IRC_SYM XenusWrite(XenusFile file, const void * buffer, size_t length, loff_t offset);
void    IRC_SYM XenusClose(XenusFile file);