#if defined(IRC_EXPORTS)
	#define IRC_SYM __attribute__ ((visibility ("default")))
#else
	#define IRC_SYM __attribute__ ((visibility ("default")))
#endif

#define XENUS_CRASH(msg) {printf("Xenus Error: %s\n", msg); exit(0);}