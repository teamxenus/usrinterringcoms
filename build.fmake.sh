g++ -c -Iinclude -Wall  -fpic -Wunused-variable -DIRC_EXPORTS src/XenusFile.cpp
g++ -c -Iinclude -Wall  -fpic -Wunused-variable -DIRC_EXPORTS src/XenusDelegatedCalls.cpp
g++ -c -Iinclude -Wall  -fpic -Wunused-variable -DIRC_EXPORTS src/XenusEP.cpp
g++ -c -Iinclude -Wall  -fpic -Wunused-variable -DIRC_EXPORTS src/XenusRegister.cpp


g++ -shared -o build/LibIRC.so XenusFile.o XenusDelegatedCalls.o XenusRegister.o XenusEP.o

rm *.o

g++ -c -Iinclude -Wall -Werror -fpic test/test.cpp
g++ test.o -pthread -Lbuild -l:LibIRC.so -o build/test.elf
chmod 777 build/test.elf

rm *.o


LD_LIBRARY_PATH=build ./build/test.elf