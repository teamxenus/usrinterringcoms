#include <cinttypes>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/syscall.h>

#include <LibIRC.h>
#include <XenusFile.h>

void XenusStartRegistration()
{
	XenusFile file;

	if (getpid() != syscall(SYS_gettid))
		XENUS_CRASH("not main thread");
		
	if (!XenusOpen(&file, uint64_t(0)))
		XENUS_CRASH("couldn't open registration file");

	XenusClose(file);
}
