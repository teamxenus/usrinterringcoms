#include <cinttypes>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <LibIRC.h>
#include <XenusFile.h>


#define CHARFS_PREFIX "XenusPsuedoFile_"

typedef struct XFile_s
{
	FILE * handle_r;
	FILE * handle_w;
} XFile_t, * XFile_p, * XFile_ref;

bool XenusOpen(XenusFile * file, const void * blob)
{
	XFile_p xfile = (XFile_p)malloc(sizeof(XFile_s));
	
	
	return true;
}

bool XenusOpen(XenusFile * file, uint64_t idx)
{
	char path[256];
	XFile_p xfile;

	xfile	= (XFile_p)malloc(sizeof(XFile_s));
	
	if (!xfile)
		return false;
	
	snprintf(path, sizeof(path), "/dev/" CHARFS_PREFIX "%llu", idx);
	xfile->handle_r = fopen(path, "rb");
	xfile->handle_w = fopen(path, "wb");
	
	if (!xfile->handle_r)
		goto error;
	
	if (!xfile->handle_w)
		goto error;
	
	*file = xfile;
	return true;
	
error:
	XenusClose(xfile);
	return false;
}

ssize_t XenusRead(XenusFile file, void * buffer, size_t length, loff_t offset)
{
	XFile_p xfile = (XFile_p)file;
	
	if (!xfile)
		return PSEUDOFILE_ERROR_ILLEGAL_PARAM;
	
	if (!buffer)
		return PSEUDOFILE_ERROR_ILLEGAL_PARAM;
	
	fseek(xfile->handle_r, offset, SEEK_SET); 
	return fread(buffer, 1, length, xfile->handle_r);
}

ssize_t XenusWrite(XenusFile file, const void * buffer, size_t length, loff_t offset)
{
	XFile_p xfile = (XFile_p)file;
	
	if (!xfile)
		return PSEUDOFILE_ERROR_ILLEGAL_PARAM;
	
	if (!buffer)
		return PSEUDOFILE_ERROR_ILLEGAL_PARAM;
	
	fseek(xfile->handle_w, offset, SEEK_SET); 
	return fwrite(buffer, 1, length, xfile->handle_w);
}

void    XenusClose(XenusFile file)
{
	XFile_p xfile = (XFile_p)file;
	if (xfile->handle_r)
		fclose(xfile->handle_r);
	if (xfile->handle_w)
		fclose(xfile->handle_w);
	free(xfile);
}