#include <cinttypes>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/syscall.h>

#include <cstring>
#include <vector>
#include <unordered_map>

#include <LibIRC.h>
#include <XenusDelegatedCalls.h>
#include <XenusFile.h>

#define sys_xenuscall 400

//#define BUILTIN_CALL_REG_BROADCAST 1
//#define BUILTIN_CALL_POL_BROADCAST 2
//#define BUILTIN_CALL_LEN_BROADCAST 4
#define BUILTIN_CALL_DB_PULL 3
#define BUILTIN_CALL_EXTENDED 4

std::unordered_map<std::string, uint64_t> delegated_calls;
pthread_mutex_t delegated_lock;

size_t XenusCall(uint8_t id, size_t a, size_t b, size_t c, size_t d)
{
	return syscall(sys_xenuscall, id, a, b, c, d);
}

void DelegatedGetFunctions()
{
#define CUR_PTR (size_t(buf) + bufidx)
	size_t length;
	void * buf;
	void * end;
	size_t bufidx;
	
	
	length = XenusCall(BUILTIN_CALL_DB_PULL, 0, 0, 0, 0);
	if (!length)
		goto out;
	
	buf = malloc(length);
	if (!buf)
		goto out;
	
	length = XenusCall(BUILTIN_CALL_DB_PULL, (size_t)buf, length, 0, 0);
	
	end = (void *)(size_t(buf) + length);
	bufidx = 0;
	
	delegated_calls.clear();
	while (end != (void *)CUR_PTR)
	{
		size_t nlen;
		uint8_t idx;
		const char * str;
		
		str = (const char *)CUR_PTR;
		nlen = strlen(str);
		bufidx += nlen + 1;
		
		idx = *(uint32_t *)CUR_PTR;
		
		bufidx += sizeof(uint32_t);
		
		delegated_calls[str] = idx;
	}
	
out:	
	if (buf)
		free(buf);
	
#undef CUR_PTR
}

void *DelegatedThreadFunctions(void *)
{
	while (1)
	{
		pthread_mutex_lock(&delegated_lock);
		DelegatedGetFunctions();
		pthread_mutex_unlock(&delegated_lock);
		sleep(60);
	}
	return nullptr;
}

void DelegatedCallsInitMutexes()
{
	if (pthread_mutex_init(&delegated_lock, NULL) != 0)
		XENUS_CRASH("couldn't create mutex");
}

void DelegatedCallsInitThreads()
{
	pthread_t fuckunix;
	
	if (pthread_create(&fuckunix, NULL, &DelegatedThreadFunctions, NULL) != 0)
		XENUS_CRASH("create functions thread");
}

void DelegatedCallsInit()
{
	DelegatedCallsInitMutexes();
	DelegatedCallsInitThreads();
}

void DelegatedCallsTryIndex()
{
	pthread_mutex_lock(&delegated_lock);
	if (delegated_calls.size() == 0)
		DelegatedGetFunctions();
	pthread_mutex_unlock(&delegated_lock);
}

bool XenusGetFunction(const char * fn, uint32_t * id)
{
	bool ret;
	ret = false;
	
	if (!id)
		return false;
	
	if (!fn)
		return false;
	
	DelegatedCallsTryIndex();
	
    pthread_mutex_lock(&delegated_lock);
	if (delegated_calls.find(fn) != delegated_calls.end())
	{
		ret = true;
		*id = delegated_calls[fn];
	}
    pthread_mutex_unlock(&delegated_lock);
	return ret;
}

uint32_t XenusCallFunctionEx(xenus_attention_sys_ref call)
{
	return (uint32_t)XenusCall(BUILTIN_CALL_EXTENDED, (size_t)call, 0, 0, 0);
}
 